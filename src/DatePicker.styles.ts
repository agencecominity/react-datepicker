import { Styles } from 'react-jss'
import { DatePickerStyles, CalendarStyles, DatePickerInputStyles } from './DatePicker.types'

const colors = {
  neutral: '#48596A',
  neutral50: '#F8FAFC',
  neutral200: '#E1E8EF',
  neutral300: '#CBD6E1',
  neutral500: '#65788B',
  neutral700: '#344556',
  primary50: '#E7EDFE',
  primary200: '#7DA1FC',
  primary300: '#5581F1',
  primary400: '#1B58F3',
}

export const CALENDAR_STYLES: Styles<CalendarStyles> = {
  calendar: {
    margin: 20
  },
  calendarHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: [10, 0],
    '&>button': {
      cursor: 'pointer',
      background: 'none',
      border: 'none',
    },
    '&>div': {
      fontWeight: 500,
      color: colors.neutral700,
    },
    '&>select': {
      borderRadius: 4,
      border: [1, 'solid', colors.neutral300],
      padding: [3, 8]
    },
  },
  calendarBody: {
    borderCollapse: 'collapse',
    '& td, & th': {
      padding: 0,
      userSelect: 'none'
    },
    '& th': {
      margin: 2,
      color: colors.neutral300,
      fontWeight: 500,
    },
    '& td>div': {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      cursor: 'pointer',
      width: 32,
      height: 28,
    }
  },
  disabled: {
    color: colors.neutral200,
    background: colors.neutral50,
  },
  today: {
    '&:not($inRange)>div': {
      backgroundColor: colors.primary50,
      color: colors.primary300,
      fontWeight: 400,
      borderRadius: '50%'
    }
  },

  inView: {},
  outOfView: {
    opacity: .5
  },
  inRangeStrict: {},
  inRange: {
    '&>div': {
      backgroundColor: colors.primary200,
      color: 'white'
    }
  },
  isStart: {
    '&>div': {
      background: colors.primary300,
      borderStartStartRadius: 4,
      borderEndStartRadius: 4,
    }
  },
  isEnd: {
    '&>div': {
      background: colors.primary300,
      borderStartEndRadius: 4,
      borderEndEndRadius: 4,
    }
  },
  isStartOnly: {},
  isEndOnly: {},
  isSelected: {},
}

export const DATEPICKER_STYLES: Styles<DatePickerStyles> = {
  ...CALENDAR_STYLES,
  activeTab: {
    '&&': {
      borderColor: colors.primary400
    }
  },
  tab: {
    borderBottom: ['solid', 1, 'transparent'],
    cursor: 'pointer',
    color: colors.neutral,
    '&$activeTab': {
      fontWeight: 600,
    }
  },
  title: {
    fontWeight: 600,
    fontSize: 18,
    color: colors.neutral,
    marginBottom: 14
  },
  body: {
    display: 'flex',
    flexDirection: 'row',
  },
  levels: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 16,
    '&>button': {
      color: colors.neutral,
      border: 'none',
      borderBottom: [1, 'solid', colors.neutral300],
      background: 'none',
      padding: 5,
      textAlign: 'center',
      cursor: 'pointer',
      flex: 1,
      '&:disabled': {
        opacity: .5,
        cursor: 'default'
      }
    }
  },
  filter: {
    display: 'flex',
    flexDirection: 'column',
    borderRight: [1, 'solid', colors.neutral300],
    paddingRight: 24,
    minWidth: 140
  },
  number: {
    display: 'flex',
    position: 'relative',
    '&>div': {
      position: 'absolute',
      right: 6,
      top: 8,
      height: 17,
      display: 'flex',
      alignItems: 'center',
      gap: 10,
      fontWeight: 500,
      color: '#94A6B8',
      pointerEvents: 'none',
    },
    '&>input': {
      '&:disabled': {
        background: colors.neutral50,
        borderColor: colors.neutral300,
        color: colors.neutral500,
      },
      '&::-webkit-outer-spin-button, &::-webkit-inner-spin-button': {
        WebkitAppearance: 'none',
        margin: 0,
      },
      appearance: 'none',
      MozAppearance: 'none',
      alignItems: 'center',
      padding: [6, 70, 6, 10],
      border: [1, 'solid', colors.neutral300],
      borderRadius: 4,
      margin: [2, 0],
      color: '#48596A',
      outlineColor: 'currentColor',
      width: 160,
    }
  },
  custom: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    '&>div':{
      margin: [4, 0]
    }
  },
  tabs: {
    display: 'flex',
    flexDirection: 'column',
    borderBottom: ['solid', 1, colors.neutral300],
    margin: [14, 0],
    alignItems: 'flex-start',
    paddingBottom: 24,
    '&>div': {
      padding: [4, 0],
      margin: [0, 12],
    }
  },
  content: {
    display: 'flex',
    flexDirection: 'row',
  },
  blockColumn: {
    display: 'flex',
    flexDirection: 'column',
    padding: [24, 24, 32],
    background: 'white',
    border: [0.5, 'solid', colors.neutral200],
    boxShadow: [2, 4, 24, 'rgba(30, 41, 59, 0.08)'],
    borderRadius: 4,
    margin: [8, 0],
  },
  btns: {
    display: 'flex',
    justifyContent: 'flex-end',
    gap: 10,
    '&>button': {
      padding: [6, 12],
      borderRadius: 4,
      cursor: 'pointer',
      '&:disabled': {
        opacity: .5,
        cursor: 'default'
      }
    }
  },
  apply: {
    color: 'white',
    background: colors.primary400,
    border: ['solid', 1, colors.primary400]
  },
  cancel: {
    background: colors.neutral50,
    border: ['solid', 1, colors.neutral200],
  }
}

export const DATEPICKERINPUT_STYLES: Styles<DatePickerInputStyles> = {
  ...DATEPICKER_STYLES,
  datePickerInput: {
    display: 'inline-flex',
    position: 'relative',
    '&:focus-within $input svg': {
      color: '#3291F5', 
    }
  },
  hide: { display: 'none' },
  show: {},
  input: {
    flex: 1,
    '& input': {
      background: 'white',
      padding: [8, 14],
      border: ['solid', 1, colors.neutral300],
      borderRadius: 4,
      minWidth: '100%',
      minHeight: '100%',
      color: 'currentcolor',
      '&:disabled': {
        color: '#94A6B8',
        borderColor: '#E1E8EF',
        background: '#F1F5F9',
      },
      '&:focus': {
        outline: '0 none',
        outlineOffset: 0,
        boxShadow: '0 0 0 0.2rem #c7d2fe',
        borderColor: '#6366F1',
      }
    },
    '& svg': {
      position: 'absolute',
      height: '100%',
      right: 9,
      color: '#C7C6CD',
    }
  },
  popup: {
    position: 'absolute',
    top: '100%',
    zIndex: 1,
  }
}