export type CalendarStyles = 'inRange' | 'inView' | 'today'
  | 'calendar' | 'calendarHeader' | 'calendarBody' | 'disabled'
  | 'outOfView' | 'inRangeStrict' | 'isStart' | 'isEnd' | 'isStartOnly' | 'isEndOnly' | 'isSelected'
export type DatePickerStyles = CalendarStyles
  | 'number' | 'tabs' | 'activeTab'
  | 'tab' | 'custom' | 'title' | 'levels' | 'body' | 'content' | 'blockColumn' | 'filter'
  | 'btns' | 'cancel' | 'apply'

export type DatePickerInputStyles = DatePickerStyles
  | 'datePickerInput' | 'input' | 'popup' | 'hide' | 'show'


export interface MomentLib {
  utc(str?: string): Moment
}
export interface Moment {
  format(str: 'dd' | 'yyyy-MM-DD' | 'MMMM' | 'DD'): string
  year(): number
  year(y?: number): Moment
  month(): number
  month(month?: number): Moment
  weekday(): number
  clone(): Moment
  add(amout: number, unit: 'week' | 'day' | 'month' | 'year'): Moment
  toDate(): Date
  diff(date: Date): number
}

