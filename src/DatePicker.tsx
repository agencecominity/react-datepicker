import { ForwardedRef, forwardRef, useEffect, useMemo, useRef, useState } from 'react'
import { Calendar, FORMAT, weekday } from './Calendar'
import { DatePickerInputStyles, DatePickerStyles, Moment, MomentLib } from './DatePicker.types'
import { ArrowIcon } from './icons/Arrow.svg'
import { CalendarIcon } from './icons/Calendar.svg'

const noop = () => { }
const useMaybeControlled = <T extends unknown>(defaultValue: T, initialValue?: T, value?: T, onChange?: (value: T) => void) => {
  const innerState = useState(initialValue)
  const controlled = value !== undefined
  const controlledRef = useRef(controlled)
  useEffect(() => {
    if (controlled && !onChange) {
      console.warn(`Component is controlled but no onChange is provided`)
    }
    if (controlledRef.current === controlled) return
    controlledRef.current = controlled
    console.error(`Component is changing behavior from ${controlled ? 'un' : ''}controlled to ${controlled ? '' : 'un'}controlled`)
  }, [controlled])
  const [state = defaultValue, setState] = controlled ? [value, noop] : innerState
  return [state, setState] as const
}

export const DatePicker = (props: {
  lib: MomentLib, canMove?: boolean, styles: Record<DatePickerStyles, string>, firstInWeek?: number,
  value?: string, initialValue?: string, onChange?: (date: string) => void, disable?: string[],
  onApply?: (date: string) => void, required?: boolean, onCancel?: () => void, isRange?: boolean
}) => {

  const { lib: moment, canMove, styles, firstInWeek = 1, value, isRange, disable, initialValue, onChange, required, onApply, onCancel } = props

  //#region states
  const tabs = [{ name: 'Day' }, { name: 'Week' }, { name: 'Month' }, { name: 'Year' }, { name: 'Cycle' }]
  const levels: { name: Level, label: string, tab?: number }[] = isRange ? [
    { name: 'last', label: 'Last' },
    { name: 'current', label: 'Current' },
    { name: 'next', label: 'Next' }
  ] : [
    { name: 'current', label: 'Today', tab: 0 },
    { name: 'last', label: 'Yesterday', tab: 0 },
    { name: 'next', label: 'Tomorrow', tab: 0 }]
  type Level = 'last' | 'current' | 'next'
  const [dateStr, directSetDate] = useMaybeControlled('', initialValue, value, onChange)
  const [filter, setFilter] = useState<{ level: Level, tab: number | null, number: number, date: string }>({
    tab: null,
    level: 'last',
    number: 1,
    date: dateStr
  })

  const setDate = (date: string) => {
    directSetDate(date)
    onChange?.(date)
  }
  const [monthView1, directSetMonthView1] = useState(() => moment.utc(`${(
    dateStr.split(':')[0] || moment.utc().format(FORMAT)
  ).slice(0, -3)}-01`))
  const [monthView2, directSetMonthView2] = useState(() => {
    const next = monthView1.clone().add(1, 'month'), toStr = dateStr.split(':')[1]?.slice(0, -3)
    const to = toStr ? moment.utc(`${toStr}-01`) : null
    return to && next.diff(to.toDate()) < 0 ? to : next
  })
  const setMonthView1 = (m: Moment, move: boolean) => {
    if (move || m.diff(monthView2.toDate()) < 0) {
      directSetMonthView1(m)
    }
    if (!move) return
    const next = m.clone().add(1, 'month')
    if (next.diff(monthView2.toDate()) > 0) directSetMonthView2(next)
  }
  const setMonthView2 = (m: Moment, move: boolean) => {
    if (move || m.diff(monthView1.toDate()) > 0) {
      directSetMonthView2(m)
    }
    if (!move) return
    const prev = m.clone().add(-1, 'month')
    if (prev.diff(monthView1.toDate()) < 0) directSetMonthView1(prev)
  }
  useEffect(() => {
    if (dateStr !== filter.date) setFilter({ ...filter, tab: null })
    if (!dateStr) return

    const [sFrom, sTo = sFrom] = dateStr.split(':')
    const [mFrom, mTo] = [sFrom, sTo].map(s => s.slice(0, -2) + '01')
    directSetMonthView1(moment.utc(mFrom))
    directSetMonthView2(moment.utc(mTo).add(mFrom == mTo ? 1 : 0, 'month'))

  }, [dateStr])
  //#region end

  //#region handlers
  const updateDate = (from: Moment, to: Moment) => {
    const [sFrom, sTo] = [from, to].map(s => s.format(FORMAT))
    const date = sFrom + (sFrom === sTo ? '' : ':' + sTo)
    setDate(date)
    return date
  }
  const handleSelect = (args: { level?: Level, tab?: number | null, number?: number }) => {
    const { level, tab, number } = { ...filter, ...args }
    let date = filter.date
    if (tab !== null) {
      const zero = '0000-01-01', idx = [, , -3, 4, 3][tab]
      const current = moment.utc(moment.utc().format(FORMAT).slice(0, idx) + (idx ? zero.slice(idx) : ''))
      if (tab === 1) { // week
        current.add(-weekday(current, firstInWeek), 'day')
      }
      const unit = (['day', 'week', 'month', 'year', 'year'] as const)[tab], one = (tab === 4 ? 10 : 1)
      current.add(one * { 'last': -number, 'next': one, 'current': 0 }[level], unit)

      date = updateDate(current, current.clone().add(
        one * number,
        unit).add(-1, 'day'))
    } else {
      setDate(dateStr)
    }
    setFilter({ level, tab, number, date })
  }
  //#region end

  return <div className={styles.blockColumn}>
    <div className={styles.title}>{isRange ? 'Date range picker' : 'Date picker'}</div>
    <div className={styles.body}>
      <div className={styles.filter}>
        {isRange && <>
          <div className={styles.levels}>
            {levels.map((t, i) => <button key={i} className={filter.level === t.name ? styles.activeTab : ''} onClick={() => handleSelect({ level: t.name })}>
              {t.label}
            </button>)}
          </div>
          <div className={styles.number}>
            <input disabled={filter.tab === null} type='number' min={1} value={filter.number} onChange={(e) => handleSelect({ number: +e.target.value })} />
            <div><span>{filter.tab === null ? null : tabs[filter.tab].name + 's'}</span><ArrowIcon /></div>
          </div>
          <div className={styles.tabs}>
            {tabs.map((t, i) => <div key={i} className={`${styles.tab} ${filter.tab === i ? styles.activeTab : ''}`} onClick={() => handleSelect({ tab: i })}>
              {t.name}
            </div>)}
          </div>
        </>}
        <div className={styles.custom}>
          {!isRange && levels.map((t, i) => <div key={i} className={`${styles.tab} ${filter.tab !== null && filter.level === t.name ? styles.activeTab : ''}`} onClick={() => handleSelect({ level: t.name, tab: 0 })}>
            {t.label}
          </div>)}
          <div className={`${styles.tab} ${filter.tab === null ? styles.activeTab : ''}`} onClick={() => handleSelect({ tab: null })}>
            Custom
          </div>
        </div>
      </div>
      <div className={styles.content}>
        <div>
          <Calendar styles={styles} canMove={canMove} maxDate={canMove ? undefined : monthView2.clone().add(-1, 'month')}
            firstInWeek={firstInWeek} isRange={isRange} disable={disable}
            lib={moment} value={dateStr} onChange={setDate} monthView={monthView1} setMonthView={setMonthView1}
          />
        </div>
        <div>
          <Calendar styles={styles} canMove={canMove} minDate={canMove ? undefined : monthView1.clone().add(1, 'month')}
            firstInWeek={firstInWeek} isRange={isRange} disable={disable}
            lib={moment} value={dateStr} isEnd onChange={setDate} monthView={monthView2} setMonthView={setMonthView2}
          />
        </div>
      </div>
    </div>
    <div className={styles.btns}>
      <button type='button' className={styles.cancel} onClick={() => onCancel?.()}>Cancel</button>
      <button type='button' className={styles.apply} disabled={required && !dateStr} onClick={() => onApply?.(dateStr)}>Apply</button>
    </div>
  </div >
}

export const DatePickerInput = forwardRef((props: {
  lib: MomentLib, canMove?: boolean, styles: Record<DatePickerInputStyles, string>, firstInWeek?: number,
  value?: string, initialValue?: string, onChange?: (date: string) => void, isRange?: boolean, disable?: string[],
  onApply?: (date: string) => void, required?: boolean, onCancel?: () => void, disabled?: boolean
} & Omit<JSX.IntrinsicElements['input'], 'onChange'>, ref: ForwardedRef<HTMLInputElement>) => {
  const { initialValue, value, onChange, onCancel, onApply, lib, canMove, firstInWeek, styles,
    isRange, required, disable, onClick, ...etc } = props
  const picker = { styles, lib, canMove, firstInWeek, isRange, required }
  const [old, setOld] = useState<string | null>(null)
  const oldRef = useRef(old)
  oldRef.current = old
  const [dateStr, setDateStr] = useMaybeControlled('', initialValue, value, onChange)
  const inputValue = useMemo(() => {
    if (!dateStr) return dateStr
    return dateStr.split(':').map(s => s.split('-').reverse().join('/')).join(' - ')
  }, [dateStr])
  const divRef = useRef<HTMLDivElement | null>(null)
  const same = old === null || old === dateStr

  const change = (v: string) => {
    setDateStr(v)
    onChange?.(v)
  }
  const cancel = () => {
    setDateStr(old ?? undefined)
    setOld(null)
    onCancel?.()
  }
  const apply = (v: string) => {
    setDateStr(v)
    setOld(null)
    onApply?.(v)
  }

  const handleClick = (e: React.MouseEvent<HTMLInputElement>) => {
    onClick?.(e)
    old === null && setOld(dateStr)
  }

  useEffect(() => {
    const handler = (event: MouseEvent) => {
      if (divRef.current && !divRef.current.contains(event.target as Node)) {
        if (same) {
          setOld(null)
        } else if (oldRef.current !== null) {
          event.preventDefault()
          event.stopPropagation()
        }
      }
    }
    document.addEventListener('click', handler, { capture: true })
    return () => {
      document.removeEventListener('click', handler, { capture: true })
    }
  }, [same])


  return <div ref={divRef} className={styles.datePickerInput}>
    <div className={styles.input}>
      <input ref={ref} value={inputValue}  {...etc} onClick={handleClick} />
      <CalendarIcon />
    </div>
    <div className={`${styles.popup} ${old === null ? styles.hide : styles.show}`}>
      <DatePicker value={dateStr} onCancel={cancel} disable={disable} onApply={apply} onChange={change} {...picker} />
    </div>
  </div>
})