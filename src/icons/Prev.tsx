export const Prev = (props: JSX.IntrinsicElements['svg']) => {

  return (
    <svg width="6" height="8" viewBox="0 0 6 8" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <style type="text/css">
        {`.st0{fill:none;stroke:currentColor;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}`}
      </style>
      <path d="M4.5 7L1.5 4L4.5 1" className="st0" />
    </svg>
  )
}
