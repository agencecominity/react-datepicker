import { DatePicker, DatePickerInput } from './DatePicker'
import { createUseStyles, Styles } from 'react-jss'
import { DATEPICKERINPUT_STYLES, DATEPICKER_STYLES } from './DatePicker.styles'
import type { DatePickerInputStyles, DatePickerStyles } from './DatePicker.types'

export const useDatePickerStyles = createUseStyles<DatePickerStyles, {}, Styles<DatePickerStyles>>(
  styles => styles
)
export const useDatePickerInputStyles = createUseStyles<DatePickerInputStyles, {}, Styles<DatePickerInputStyles>>(
  styles => styles
)
type ObjOrFn<T> = T | ((v: T) => T)
const asFn = <T extends object>(objOrFn?: T | ObjOrFn<T>): (v: T) => T => {
  if (typeof objOrFn === 'function') return objOrFn
  return objOrFn ? () => objOrFn : v => v
}

export const StyledDatePicker = (props: Omit<Parameters<typeof DatePicker>[0], 'styles'> & {
  styles?: ObjOrFn<Styles<DatePickerStyles, {}, undefined>>
}) => {
  const styles = useDatePickerStyles({ theme: asFn(props.styles)(DATEPICKER_STYLES) })
  return <DatePicker {...{ ...props, styles }} />
}

export const StyledDatePickerInput = (props: Omit<Parameters<typeof DatePickerInput>[0], 'styles'> & {
  styles?: ObjOrFn<Styles<DatePickerInputStyles, {}, undefined>>
}) => {
  const styles = useDatePickerInputStyles({ theme: asFn(props.styles)(DATEPICKERINPUT_STYLES) })
  return <DatePickerInput {...{ ...props, styles }} />
}