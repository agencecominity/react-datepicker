import { useEffect, useMemo } from 'react'

import { Next } from './icons/Next'
import { Prev } from './icons/Prev'
import { CalendarStyles, Moment, MomentLib } from './DatePicker.types'

export const FORMAT = 'yyyy-MM-DD'

export const weekday = (date: Moment, firstInWeek: number) => (date.weekday() + 7 - firstInWeek) % 7

export const Calendar = (props: {
  lib: MomentLib, isEnd?: boolean, canMove?: boolean, disable?: string[],
  value?: string, isRange?: boolean, onChange: (date: string) => void,
  monthView: Moment, setMonthView: (m: Moment, move: boolean) => void,
  styles: Partial<Record<CalendarStyles, string>>, required?: boolean
  maxDate?: Moment, minDate?: Moment, firstInWeek?: number
}) => {

  const {
    value: dateStr, onChange, canMove = false, required, disable,
    isRange, lib: moment, isEnd, minDate, maxDate, monthView, setMonthView,
    styles, firstInWeek = 1 // Monday
  } = props
  const toDay = moment.utc(moment.utc().format(FORMAT)).toDate()
  const [startStr, endStr = startStr] = (dateStr ?? '').split(':')
  const selectedDates = [startStr, endStr].map(str => str ? moment.utc(str) : null)
  const [start, end = start] = selectedDates

  //# region states
  const monthStr = (isEnd ? endStr : startStr).slice(0, -3)
  const yearView = monthView.year(), minYear = minDate?.year(), maxYear = maxDate?.year()
  const firstInView = useMemo(() => {
    return monthView.clone().add(-weekday(monthView, firstInWeek), 'day')
  }, [monthView])

  const yearDiff = canMove ? 10 : Math.max(Math.min(yearView - (minYear ?? 0), 10), 19 - ((maxYear ?? (yearView + 20)) - yearView))
  const years = Array(20).fill(0).map((_, i) => yearView - yearDiff + i)
  //#endregion

  //#region hooks
  useEffect(() => {
    // if (monthStr && !(isEnd && endStr === startStr)) setMonthView(moment.utc(`${monthStr}-01`), false)
  }, [monthStr])
  //#endregion

  //#region handlers
  const handleMonthViewChange = (i: number) => {
    setMonthView(monthView.clone().add(i, 'month'), canMove)
  }
  const handleChange = (dStr: string, inRange: boolean, afterStart: number) => {
    if (dateStr) {
      if (inRange) {
        if (endStr === startStr && !required) dStr = ''
      } else if (isRange && !disable?.some(d => afterStart < 0 ? dStr < d && d < startStr : dStr > d && d > endStr)) {
        dStr = afterStart < 0 ? dStr + ':' + endStr : startStr + ':' + dStr
      }
    }
    onChange(dStr)
  }
  //#endregion

  //#region view
  const joinStyles = (styles: (string | undefined | false)[]) => styles.filter(x => x).join(' ')

  return <div tabIndex={-1} className={styles.calendar}>
    <div className={styles.calendarHeader}>
      <button type='button' disabled={minDate && monthView.diff(minDate.toDate()) <= 0} onClick={() => handleMonthViewChange(-1)}>
        <Prev />
      </button>
      <div>
        {monthView.format('MMMM')}
      </div>
      <select value={yearView} onChange={e => setMonthView(monthView.clone().year(+e.target.value), canMove)}>
        {years.map((y, i) => <option value={y} key={i}>{y}</option>)}
      </select>
      <button type='button' disabled={maxDate && monthView.diff(maxDate.toDate()) >= 0} onClick={() => handleMonthViewChange(1)}>
        <Next />
      </button>
    </div>
    <table className={styles.calendarBody}>
      <thead>
        <tr>{Array(7).fill(0).map((_, i) =>
          <th key={i}>{firstInView.clone().add(i, 'day').format('dd')}</th>
        )}</tr>
      </thead>
      <tbody>{Array(6).fill(0).map((_, j) =>
        <tr key={j}>{Array(7).fill(0).map((_, i) => {
          const day = firstInView.clone().add(j * 7 + i, 'day'), dayStr = day.format('yyyy-MM-DD')
          const afterStart = start ? day.diff(start.toDate()) : -1, beforeEnd = end ? end.diff(day.toDate()) : -1
          const inRange = afterStart >= 0 && beforeEnd >= 0
          const isToday = !day.diff(toDay)
          const disabled = disable?.some(d => {
            const [from, e = from] = d.split(':'), to  = e || '9999-12-31'
            return from <= dayStr && dayStr <= to
          })
          return <td className={joinStyles([
            day.month() === monthView.month() ? styles.inView : styles.outOfView,
            inRange && styles.inRange,
            afterStart > 0 && beforeEnd > 0 && styles.inRangeStrict,
            !afterStart && styles.isStart,
            !beforeEnd && styles.isEnd,
            disabled && styles.disabled,
            beforeEnd > 0 && !afterStart && styles.isStartOnly,
            afterStart > 0 && !beforeEnd && styles.isEndOnly,
            !afterStart && !beforeEnd && styles.isSelected,
            isToday && styles.today
          ])} key={i} >
            <div tabIndex={-1} onClick={() => disabled || handleChange(day.format(FORMAT), inRange, afterStart)}>
              {day.format('DD')}
            </div>
          </td>
        })}</tr>
      )}</tbody>
    </table>
  </div>
  //#endregion
}


